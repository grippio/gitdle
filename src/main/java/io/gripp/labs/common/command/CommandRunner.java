package io.gripp.labs.common.command;

public interface CommandRunner {
    String[] LATEST_TAG = {"git", "describe", "--abbrev=0", "--tags"};
    String[] TAG_AT_HEAD = {"git", "tag", "--points-at", "HEAD"};
    String[] CURRENT_BRANCH = {"git", "rev-parse", "--abbrev-ref", "HEAD"};
    String[] CURRENT_REF = {"git", "show-ref", "--hash", "HEAD"};

    CommandResponse runCommand(String[] commands);
    CommandResponse runGitRefCommand(String reference);

    interface Factory {
        default CommandRunner create() {
            return new CommandRunnerImpl();
        }
    }
}

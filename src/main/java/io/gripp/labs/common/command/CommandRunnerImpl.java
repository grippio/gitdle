package io.gripp.labs.common.command;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

class CommandRunnerImpl implements CommandRunner {

    private static String[] GIT_REF = {"git", "show-ref", "--hash"};

    @Override
    public CommandResponse runGitRefCommand(String reference) {
        return runCommand(append(GIT_REF, reference));
    }

    @Override
    public CommandResponse runCommand(String[] commands) {
        CommandResponse response = new CommandResponse();

        try {
            Process process = Runtime.getRuntime().exec(commands);

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            response.setResponse(reader.readLine());
            response.setErrorResponse(errorReader.readLine());

        } catch (Exception ex) {
            response.setErrorResponse(ex.getMessage());
        }

        return response;
    }

    private  <T> T[] append(T[] arr, T element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }
}

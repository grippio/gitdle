package io.gripp.labs.versioning;

import org.gradle.api.Project;

public interface GitVersionUtil {
    void versionProject();

    interface Factory {
        default GitVersionUtil create(Project project, GitVersionExtension versionExtension) {
            return new GitVersionUtilImpl(project, versionExtension);
        }
    }
}

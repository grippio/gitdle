package io.gripp.labs.versioning;

public class GitVersionExtension {
    public static String BRANCH = "BRANCH";

    private boolean versionWithTag = false;
    private String versionRegex;
    private String versionPrefix;
    private String versionSuffix;
    private String defaultVersion = BRANCH;

    public void setVersionWithTag(Boolean versionWithTag) {
        this.versionWithTag = versionWithTag;
    }

    public String getVersionRegex() {
        return versionRegex;
    }

    public void setVersionRegex(String versionRegex) {
        this.versionRegex = versionRegex;
    }

    public String getDefaultVersion() {
        return defaultVersion;
    }

    public void setDefaultVersion(String defaultVersion) {
        this.defaultVersion = defaultVersion;
    }

    public boolean isVersionWithTag() {
        return versionWithTag;
    }

    public void setVersionWithTag(boolean versionWithTag) {
        this.versionWithTag = versionWithTag;
    }

    public String getVersionPrefix() {
        return versionPrefix;
    }

    public void setVersionPrefix(String versionPrefix) {
        this.versionPrefix = versionPrefix;
    }

    public String getVersionSuffix() {
        return versionSuffix;
    }

    public void setVersionSuffix(String versionSuffix) {
        this.versionSuffix = versionSuffix;
    }
}

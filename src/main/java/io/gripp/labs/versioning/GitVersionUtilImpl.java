package io.gripp.labs.versioning;

import io.gripp.labs.util.Strings;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtraPropertiesExtension;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.gripp.labs.properties.GitPropertyNames.*;
import static io.gripp.labs.versioning.GitVersionExtension.BRANCH;

class GitVersionUtilImpl implements GitVersionUtil {

    private final String BLANK = "";

    private final Project project;
    private final GitVersionExtension versionExtension;

    GitVersionUtilImpl(Project project,
                       GitVersionExtension versionExtension) {
        this.project = project;
        this.versionExtension = versionExtension;
    }

    @Override
    public void versionProject() {
        if (shouldUseTagAsVersion()) {
            project.setVersion(resolvedVersion((String) project.getExtensions().getExtraProperties().get(GIT_LAST_TAG)));
        } else {
            project.setVersion(getDefaultVersion());
        }

        System.out.println("Setting Version: " + project.getVersion());

        project.getSubprojects().forEach(project1 -> {
            project1.setVersion(project.getVersion());
            System.out.println(project1.getName() + " version: " + project1.getVersion());
        });
    }

    private boolean shouldUseTagAsVersion() {
        ExtraPropertiesExtension propertiesExtension = project.getExtensions().getExtraProperties();

        return !BLANK.equals(propertiesExtension.get(GIT_LAST_TAG))
                && !BLANK.equals(propertiesExtension.get(GIT_HASH))
                && !BLANK.equals(propertiesExtension.get(GIT_LAST_TAG_HASH))
                && !BLANK.equals(propertiesExtension.get(GIT_TAG_AT_HEAD))
                && propertiesExtension.get(GIT_LAST_TAG).equals(propertiesExtension.get(GIT_TAG_AT_HEAD))
                && tagMatchesRegex();
    }

    private boolean tagMatchesRegex() {
        String tag = (String) project.getExtensions().getExtraProperties().get(GIT_LAST_TAG);

        if (versionExtension.getVersionRegex() != null) {
            Pattern pattern = Pattern.compile(versionExtension.getVersionRegex());
            Matcher matcher = pattern.matcher(tag);

            return matcher.matches();
        }

        return true;
    }

    private String resolvedVersion(String rawVersion) {
        if (!Strings.isNullOrEmpty(versionExtension.getVersionPrefix())) {
            rawVersion = rawVersion.replaceFirst(versionExtension.getVersionPrefix(), "");
        }

        if (!Strings.isNullOrEmpty(versionExtension.getVersionSuffix())) {
            rawVersion = rawVersion.replaceFirst(versionExtension.getVersionSuffix(), "");
        }

        return rawVersion;
    }

    private String getDefaultVersion() {
        if (versionExtension.getDefaultVersion().equals(BRANCH)) {
            return (String) project.getExtensions().getExtraProperties().get(GIT_BRANCH);
        } else {
            return versionExtension.getDefaultVersion();
        }
    }
}

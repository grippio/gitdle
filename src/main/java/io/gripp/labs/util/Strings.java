package io.gripp.labs.util;

public class Strings {

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.length() == 0;
    }
}

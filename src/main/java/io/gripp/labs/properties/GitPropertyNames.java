package io.gripp.labs.properties;

public interface GitPropertyNames {
    String GIT_BRANCH = "gitBranch";
    String GIT_HASH = "gitHash";
    String GIT_LAST_TAG = "gitLastTag";
    String GIT_LAST_TAG_HASH = "gitLastTagHash";
    String GIT_TAG_AT_HEAD = "gitTagAtHead";
}

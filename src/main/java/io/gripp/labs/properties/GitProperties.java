package io.gripp.labs.properties;

import io.gripp.labs.common.command.CommandResponse;
import io.gripp.labs.common.command.CommandRunner;
import io.gripp.labs.versioning.GitVersionExtension;
import io.gripp.labs.versioning.GitVersionUtil;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtraPropertiesExtension;

import javax.inject.Inject;
import java.util.Optional;

import static io.gripp.labs.properties.GitPropertyNames.*;

public class GitProperties implements Plugin<Project> {

    private final CommandRunner commandRunner;
    private final GitVersionUtil.Factory versionFactory;

    @Inject
    GitProperties() {
        CommandRunner.Factory factory = new CommandRunner.Factory() {};

        versionFactory = new GitVersionUtil.Factory() {};
        commandRunner = factory.create();
    }

    @Override
    public void apply(Project project) {
        GitVersionExtension tagsExtension = project.getExtensions().create("gitVersion", GitVersionExtension.class);
        GitVersionUtil versionUtil = versionFactory.create(project, tagsExtension);

        setGitProperties(project);

        project.afterEvaluate(task -> {
            if (tagsExtension.isVersionWithTag()) {
                versionUtil.versionProject();
            }
        });
    }

    private void setGitProperties(Project project) {
        ExtraPropertiesExtension propertiesExtension = project.getExtensions().getExtraProperties();
        propertiesExtension.set(GIT_BRANCH, getCurrentBranch().orElse(""));
        propertiesExtension.set(GIT_HASH, getCurrentHash().orElse(""));
        propertiesExtension.set(GIT_LAST_TAG, getLastTag().orElse(""));
        propertiesExtension.set(GIT_TAG_AT_HEAD, getTagAtHead().orElse(""));
        propertiesExtension.set(GIT_LAST_TAG_HASH, getHashForReference((String) propertiesExtension.get(GIT_LAST_TAG)).orElse(""));
    }

    private Optional<String> getCurrentBranch() {
        CommandResponse commandResponse = commandRunner.runCommand(CommandRunner.CURRENT_BRANCH);

        if (commandResponse.getResponse() != null) {
            System.out.println("Current Branch: " + commandResponse.getResponse());
        }

        return Optional.ofNullable(commandResponse.getResponse());
    }

    private Optional<String> getCurrentHash() {
        CommandResponse commandResponse = commandRunner.runCommand(CommandRunner.CURRENT_REF);

        if (commandResponse.getResponse() != null) {
            System.out.println("Current Reference: " + commandResponse.getResponse());
        }

        return Optional.ofNullable(commandResponse.getResponse());
    }

    private Optional<String> getLastTag() {
        CommandResponse commandResponse = commandRunner.runCommand(CommandRunner.LATEST_TAG);

        if (commandResponse.getResponse() != null) {
            System.out.println("Latest Tag: " + commandResponse.getResponse());
        }

        return Optional.ofNullable(commandResponse.getResponse());
    }

    private Optional<String> getHashForReference(String reference) {
        CommandResponse commandResponse = commandRunner.runGitRefCommand(reference);

        if (commandResponse.getResponse() != null) {
            System.out.println(reference + " hash: " + commandResponse.getResponse());
        }

        return Optional.ofNullable(commandResponse.getResponse());
    }

    private Optional<String> getTagAtHead() {
        CommandResponse commandResponse = commandRunner.runCommand(CommandRunner.TAG_AT_HEAD);

        if (commandResponse.getResponse() != null) {
            System.out.println("Tag at HEAD: " + commandResponse.getResponse());
        }

        return Optional.ofNullable(commandResponse.getResponse());
    }
}
